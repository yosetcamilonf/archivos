/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package View;

import Negocio.SIA;

/**
 *
 * @author DOCENTE
 */
public class TestSIA {
    public static void main(String[] args) {
        String fisica= "https://gitlab.com/jdavid26/csv-archivos/-/raw/main/CSV_F%C3%8DSICA.csv";
        String estructuras= "https://gitlab.com/jdavid26/csv-archivos/-/raw/main/CSV_ESTRUCTURA.csv";
        String poo= "https://gitlab.com/jdavid26/csv-archivos/-/raw/main/CSV_POO2.csv";
        
//        https://gitlab.com/pruebas_madarme/persistencia/ed_a/sia-a/-/raw/main/notas0a5.csv?ref_type=heads

        SIA sia=new SIA(fisica, estructuras, poo); 
        //System.out.println(sia.getListadoFisica());
        System.out.println("Estudiantes de Física\n"+sia.getListadoFisica()+"\n"+"Estudiantes de Estructura de datos\n"
                +sia.getListadoEstructuras()+"\n"+"Estudiantes de POO2\n"+sia.getListadoPoo());
        System.out.println(sia.getListadoFinal().toString());
        sia.getInformePDF();
    }
}
